#!/bin/sh

if sudo docker run --rm $(sudo docker build -q .) ; then
	echo "OK"
else
	echo "Failed"
	exit 1
fi
