#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./start.sh <PORT>"
	exit 1
fi

TEST_PORT=$1

sudo docker run --name mynginx1 -p 8080:80 -v $(pwd)/files:/usr/share/nginx/html:ro -d nginx
